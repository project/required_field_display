# Required Field Display

Simple Drupal module to display required fields on the manage fields screen.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/required_field_display).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/required_field_display).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

No configuration is needed.


## Maintainers

- Rob Ballou - [rballou](https://www.drupal.org/u/rballou)
- Brian Gallagher - [diamondsea](https://www.drupal.org/u/diamondsea)
- Viktor Holovachek - [AstonVictor](https://www.drupal.org/u/astonvictor)
