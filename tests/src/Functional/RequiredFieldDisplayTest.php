<?php

namespace Drupal\Tests\required_field_display\Functional;

use Drupal\Tests\node\Functional\NodeTestBase;

/**
 * Class RequiredFieldDisplayTest. The base class for testing required fields.
 */
class RequiredFieldDisplayTest extends NodeTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['node', 'field_ui', 'required_field_display'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test private fields.
   */
  public function testPrivateFields() {
    // Log in as an admin user with permission to manage node settings.
    $admin = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($admin);

    // Create a new node type.
    $this->createContentType(['type' => 'required']);

    // Check the status of the page.
    $this->drupalGet('admin/structure/types/manage/required/form-display');
    $this->assertSession()->statusCodeEquals(200);

    // Check if required selector exist on the page.
    $this->assertSession()->elementExists('css', '.field-display-overview .required-field');
  }

}
